KeyCRM WordPress Plugin

## Порядок обновления из модуля https://github.com/keycrm/woocommerce-module

Разворачиваем модуль ритейла и в файле `wp-content/plugins/woo-keycrm/include/function.php` добавляем на строке 2 и кладем рядом файлик `keycrm-funcs.php`
`include 'keycrm-funcs.php';`

Далее в файлике `wp-content/plugins/woo-keycrm/include/api/class-wc-keycrm-request.php` перед строкой
`if (self::METHOD_GET === $method && count($parameters)) {`
добавляем следующий блок

```
        if (!allowedPath($path)){
            return new WC_Keycrm_Response(777);
        }
        $parameters = globalConvert_kcrm($this->url, $path, $method, $parameters); //добавлено
        if (isset($parameters['url'])){
            $apiKey = $this->defaultParameters['apiKey'];
            $urlArr = explode('/api/', $this->url);
            $url = trim($urlArr[0], '/api').$parameters['url'];
            unset($parameters['apiKey'], $parameters['url']);
            if (isset ($parameters['order'])) {
                $parameters = $parameters['order'];
            }
        }
```

На этом сборка плагина после обновления модуля ритейла закончена. Делается это разово для того, чтобы подхватить новые корректировки. Если будут критичные изменения в API ритейла в части названия методов их структуры и т.д., то естественно, это все нужно править.
Косметические работы по изменению названий id шников и скрытию полей проводятся в файле
`woo-keycrm/include/abstracts/class-wc-keycrm-abstracts-settings.php`
