<?php

/**
 * PHP version 5.3
 *
 * WC_Keycrm_Exception_Curl class
 *
 * @category KeyCRM
 * @package  WC_Keycrm_Exception_Curl
 * @author   KeyCRM <dev@keycrm.app>
 * @license  https://opensource.org/licenses/MIT MIT License
 * @link     http://keycrm.app/docs/Developers/ApiVersion4
 */
class WC_Keycrm_Exception_Curl extends \RuntimeException
{
}
