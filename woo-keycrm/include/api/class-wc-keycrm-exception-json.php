<?php

/**
 * PHP version 5.3
 *
 * WC_Keycrm_Exception_Json class
 *
 * @category KeyCRM
 * @package  WC_Keycrm_Exception_Json
 * @author   KeyCRM <dev@keycrm.app>
 * @license  https://opensource.org/licenses/MIT MIT License
 */

class WC_Keycrm_Exception_Json extends \DomainException
{
}
